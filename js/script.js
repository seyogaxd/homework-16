function renderList(array, parent = document.body) {
    const list = document.createElement("ul");
    array.forEach(item => {
        const listItem = document.createElement("li");
        if (Array.isArray(item)) {
            renderList(item, listItem);
        } else {
            listItem.textContent = item;
        }
        list.appendChild(listItem);
    });

    parent.appendChild(list);
}
const userArray = ["Kharkiv", "Kiev", ["Borispol", "Irpin"], "Odessa", ["Lviv", "Zaporozhye"], "Dnieper"];
renderList(userArray);

let seconds = 3;
const countdownInterval = setInterval(() => {
    seconds--;
    if (seconds < 0) {
        clearInterval(countdownInterval);
        document.body.innerHTML = '';
    }
}, 1000);
